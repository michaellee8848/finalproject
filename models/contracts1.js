/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('contracts1', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    buyer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    originator_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    asset_type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    value: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    expected_ship_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    contract_url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    loan_no: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fv: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    date_start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_end: {
      type: DataTypes.DATE,
      allowNull: true
    },
    interest_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    lc_no: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lc_bankname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lc_bank_swiftcode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    no_investor: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    investor_group: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('1','2','3'),
      allowNull: true
    },
    details: {
      type: DataTypes.STRING,
      allowNull: true
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: true
    },
    shipment_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    kyc_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'contracts1'
  });
};
