/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('invested_amount', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    investor_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    loan_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    invested_amount: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    payout_amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'invested_amount'
  });
};
