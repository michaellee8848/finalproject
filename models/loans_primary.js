/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('loans_primary', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    loan_no: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fv: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    date_start: {
      type: DataTypes.DATE,
      allowNull: false
    },
    date_end: {
      type: DataTypes.DATE,
      allowNull: false
    },
    originator_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    buyer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    interest_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    lc_no: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lc_bankname: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lc_bank_swiftcode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    no_investor: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    investor_group: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('1','2','3'),
      allowNull: true
    },
    details: {
      type: DataTypes.STRING,
      allowNull: true
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: true
    },
    remain_amt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    originator_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    buyer_name: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'loans_primary'
  });
};
