/**
 * Created by Michael on 2016/8/22.
 */
var LocalStrategy = require("passport-local").Strategy;
var models = require('./models/index');
var bcrypt   = require('bcrypt-nodejs');

var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/config.json')[env];

//Setup local strategy
module.exports = function (app, passport) {
    function authenticate(username, password, done) {

        models.admin_users.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if(!result){
                return done(null, false);
            }else{
                console.log("found user")
                if(bcrypt.compareSync(password , result.encrypted_password)){
                    return done(null, result);
                }else{
                    console.log("passwords don't match")
                    return done(null, false);
                }
            }
        }).catch(function(err){
            return done(err, false);
        });
    }

    function verifyCallback(accessToken, refreshToken, profile, done) {
        if(profile){
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            models.users.findOrCreate({where: {email: email}, defaults: {email: email, name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    models.authentication_provider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else{
            done(null, false);
        }
    }

   

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        console.log("attaching user to request", user)
        models.admin_users.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }else{
                done(new Error("User Not Found"));
            }
        }).catch(function(err){
            done(err);
        });
    });

};