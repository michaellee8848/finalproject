/**
 * Created by Michael on 2016/8/1.
 */
var path = require("path");
var express = require("express");
var app = express();

var passport = require("passport");
var bodyParser = require("body-parser");
var session = require("express-session");
var flash    = require('connect-flash');
var cookieParser = require('cookie-parser');

const  PORT = "port";

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// Initialize session
app.use(session({
    secret: "blockchain-trade-finance",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

// connect flash
app.use(flash());
app.post("/login", passport.authenticate("local", {
    successRedirect: "/status/201",
    failureRedirect: "/status/403"
}));
require("./auth")(app, passport);
require('./routes.js')(app); // load our routes and pass in our app and fully configured passport

app.get("/status/:code", function(req, res) {
    console.log("Received response code", req.params.code);
    var code = req.params.code;
    res.status(code).end()
});

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});