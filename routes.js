var express = require("express");
var bcrypt = require('bcrypt-nodejs');
var passport = require('passport');
var q = require("q");
var models = require("./models/index");
var randomstring = require("randomstring");
var async = require("async");

module.exports = function (app) {

    app.post("/api/loan/", function (req, res) {

        var originator_name = null;
        var buyer_name = null;

        async.waterfall([
            function (callback) {
                models.admin_users.findOne({where: {id: req.user.id}})
                    .then(function (result) {
                        //res.status(200).json(result);
                        //console.info(result.company);
                        originator_name = result.company;
                        callback(null, result.company);
                    }).catch(function (err) {
                    console.error(err);
                    res.status(500).end();
                })
            },
            function (result, callback) {
                models.admin_users.findOne({where: {id: req.body.buyer_id}})
                    .then(function (result) {
                        //res.status(200).json(result);
                        buyer_name = result.company;
                        callback(null, result.company);
                    }).catch(function (err) {
                    console.error(err);
                    res.status(500).end();
                })
            },
            function (result, callback) {
                models.loans_primary.create(
                    {
                        fv: req.body.fv,
                        remain_amt: req.body.fv,
                        date_start: req.body.date_start,
                        date_end: req.body.date_end,
                        buyer_id: req.body.buyer_id,
                        interest_rate: req.body.interest_rate,
                        lc_no: req.body.lc_no,
                        lc_bankname: req.body.lc_bankname,
                        lc_bank_swiftcode: req.body.lc_bank_swiftcode,
                        details: req.body.details,
                        originator_id: req.user.id,
                        originator_name: originator_name,
                        buyer_name: buyer_name,
                        loan_no: randomstring.generate(15),
                        currency: req.body.currency
                    }
                ).then(function (loan) {
                    //console.info(loan);
                    callback(null, loan);
                })
            }

        ], function (err, data) {
            //console.info(data);
            //res.status(200);
            res.status(200).json(data)
            //returnResults(data, res);
        });
    });


    app.post("/api/user/", function (req, res) {
        models.admin_users.create(
            {
                email: req.body.email,
                encrypted_password: bcrypt.hashSync(req.body.password),
                createdAt: new Date(),
                company: req.body.company,
                company_type: req.body.companyType
            }
        ).then(function (user) {
            //console.info(user);
            res.send(user);
        });
    });

    app.get("/api/loan/list", function (req, res) {
        models.loans_primary.findAll({
            where:{
                remain_amt: {
                    $gt: 0
                }
            }

        })
            .then(function (loans) {
                // console.info(loans);
                res.json(loans);
            });
    });

    app.get("/api/loan/loandetails", function (req, res) {
        var loanId = req.params.loanId;
        models.contracts.findById(loanId)
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });


    app.get("/api/buyers", function (req, res) {
        console.info("buyers ----");
        models.admin_users.findAll({
            where: {
                company_type: 2
            }
        })
            .then(function (buyers) {
                console.info(buyers);
                res.status(200).json(buyers );
            });
    });

    app.get("/api/loan/successfulloan", function (req, res) {
        //var status = req.body.status;
        models.contracts.findAll({
            where: {
                status: {
                    $eq: 1
                }
            }
        })
            .then(function (loans) {
                res.json(loans);
            });
    });

    app.get("/api/loan/badloan", function (req, res) {
        //var status = req.body.status;
        models.contracts.findAll({
            where:{
                status: {
                    $eq: 2
                }
            }
        })
            .then(function (loans) {
                res.json(loans);
            });
    });

    app.get("/api/loan/listmatched", function (req, res) {

        var remain_amt = req.body.remain_amt;
        models.contracts.findAll({
            where: {remain_amt: 0}
        })
            .then(function (loans) {
                res.json(loans);
            });
    });

    app.post("/api/loan/search", function (req, res) {
        //console.info(" search by ");
        models.loans_primary.findAll({
            where: {
                $or: [{originator_name: req.body.query}, {buyer_name: req.body.query},
                    {currency: req.body.query}, {interest_rate: req.body.query}]
            }
        })
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });

    app.get("/logout", function (req, res) {
        console.info("logout ....");
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.redirect("/")
    });

    app.get("/api/loan/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        // get the data from db  and return in res
        models.loans_primary.findById(loanId)
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });

    app.get("/api/contract/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        // get the data from db  and return in res
        console.info(loanId)
        models.contracts.findOne({where: {loan_id: loanId}})
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });

    app.get("/api/investor/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        // get the data from db  and return in res
        models.invested_amount.findOne({where: {loan_id: loanId}})
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });


    app.get("/api/investors/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        // get the data from db  and return in res
        models.invested_amount.findAll({where: {loan_id: loanId}})
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });

    app.get("/api/loan/transactions/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        // get the data from db  and return in res
        models.invested_amount.count({where: {loan_id : loanId}})
            .then(function (results) {
                console.info(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });

    app.post("/api/loan/:loanId", function (req, res) {
        var loanId = req.params.loanId;
        console.info(req.body.reduced_amt);
        var totalAmountInvested = 0;
        var totalNumberOfInvestedtrx = 0;
        models.loans_primary.findById(loanId)
            .then(function (loan) {
                // Check if record exists in db
                if (loan) {
                    var faceValue = loan.fv;
                    models.invested_amount.create(
                    { investor_id: req.user.id,  loan_id: loanId, invested_amount: req.body.reduced_amt, currency: loan.currency})
                        .then(function(investedAmt) {
                            models.invested_amount.findAll({
                                where: {
                                    loan_id: loanId
                                }
                            })
                                .then(function (results) {
                                    //console.info(results);
                                    results.forEach(function(item, index){
                                        console.info(" yyyy invested amt id "  + investedAmt.id);
                                        console.info("yyy item id "  + item.id);

                                        if(item.id == investedAmt.id){
                                            totalAmountInvested = totalAmountInvested + item.invested_amount;
                                            totalNumberOfInvestedtrx = results.length + 1;
                                            console.info("invested xxxxyyy" + totalNumberOfInvestedtrx);
                                        }

                                    });
                                    console.info("TOTAL INVESTED AMT : " + totalAmountInvested);
                                    console.info(faceValue);
                                    if(totalAmountInvested > faceValue){
                                        console.log("Total invested amount is more than face value");
                                        res.status(500).end();
                                    }else{
                                        remainingAmt = loan.remain_amt - totalAmountInvested;
                                        console.info("REMAINING AMOUNT : " + remainingAmt);
                                        loan.updateAttributes({
                                            remain_amt: remainingAmt
                                        }).then(function () {
                                            // insert record in invested_amount table
                                            res.status(201).json({
                                                amountInvested: remainingAmt,
                                                totalNumberOfInvestedtrx : totalNumberOfInvestedtrx
                                            })
                                        }).catch(function (err) {
                                            res.status(500).json(err)
                                        })
                                    }

                                })
                                .catch(function (err) {
                                    console.log(err);
                                    res.status(500).end();
                                });
                        }).error(function(error){
                        console.info(error);
                        res.status(500).end();
                    });

                }
            })
    });


    app.get("/api/user", function (req, res) {
        models.admin_users.create(
            {
                updatedAt: new Date()

            }
        ).then(function (user) {
            res.send(user);
        });
    });


};

