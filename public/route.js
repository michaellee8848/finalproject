(function () {

    angular
        .module("ChainApp")
        .config(ChainConfig);

    function ChainConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("landing", {
                url: "/landing",
                templateUrl: "/views/landing.html",
                controller: "LandingCtrl as ctrl"
            })
            .state("login", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: "LoginCtrl as ctrl"
            })
            .state("registration", {
                url: "/registration",
                templateUrl: "/views/registration.html",
                controller: "RegistrationCtrl as ctrl"
            })
            .state("regisconfirm", {
                url: "/registerconfirm",
                templateUrl: "/views/registerconfirm.html",
                controller: "RegisterconfirmCtrl as ctrl"
            })
            .state("badloan", {
                url: "/badloan",
                templateUrl: "/views/badloan.html",
                controller: "BadLoanCtrl as ctrl"
            })
            .state("loanconfirmation", {
                url: "/loanconfirmation",
                templateUrl: "/views/loanconfirmation.html",
                controller: "LoanConfirmationCtrl as ctrl"
            })
            .state("editloan", {
                url: "/editloan",
                templateUrl: "/views/editloan.html",
                controller: "EditLoanCtrl as ctrl"
            })
            .state("matchedloan", {
                url: "/matchedloan",
                templateUrl: "/views/matchedloan.html",
                controller: "MatchedLoanCtrl as ctrl"
            })
            .state("successfulloan", {
                url: "/successfulloan",
                templateUrl: "/views/successfulloan.html",
                controller: "SuccessfulLoanCtrl as ctrl"
            })
            .state("unmatchedloan", {
                url: "/unmatchedloan",
                templateUrl: "/views/unmatchedloan.html",
                controller: "UnmatchedLoanCtrl as ctrl"
            })
            .state("createloan", {
                url: "/createloan",
                templateUrl: "/views/createloan.html",
                controller: "CreateLoanCtrl as ctrl"
            })
            .state("loandetails", {
                url: "/loandetails",
                templateUrl: "/views/loandetails.html",
                controller: "LoanDetailsCtrl as ctrl"
            })
            .state("contractdetails", {
                url: "/contractdetails/:loanId",
                templateUrl: "/views/contractdetails.html",
                controller: "ContractCtrl as ctrl"
            })
            .state("hittheloan", {
                url: "/hittheloan/:loanId",
                templateUrl: "/views/hittheloan.html",
                controller: "ViewLoanCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/landing");
    }

    ChainConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

})();
