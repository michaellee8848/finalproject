/**
 * Created by Michael on 2016/8/20.
 */
(function () {
    angular.module("ChainApp")
        .service("loanService", loanService);

    loanService.$inject = ["$http", "$q"];

    function loanService($http, $q) {
        var service = this;

        service.createLoan = function (loan, callback){
            var defer = $q.defer();
            $http.post("/api/loan/", loan)
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };
        
    }
})();
