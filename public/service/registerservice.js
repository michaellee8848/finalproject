(function () {
    angular.module("ChainApp")
        .service("registerService", registerService);

    registerService.$inject = ["$http", "$q"];

    function registerService($http, $q) {
        var service = this;
        
        console.log("--in registerService ---");
        
        service.createUser = function (user){
            var defer = $q.defer();
            
            console.log("---in registerService.createUser ---");
            console.log("user info to be created: " + JSON.stringify(user));
            $http.post("/api/user/", user)
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

    }
})();

