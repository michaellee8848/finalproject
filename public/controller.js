(function () {
    angular.module("ChainApp")
        .controller("LandingCtrl", LandingCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("CreateLoanCtrl", CreateLoanCtrl)
        .controller("LoanDetailsCtrl", LoanDetailsCtrl)
        .controller("MatchedLoanCtrl", MatchedLoanCtrl)
        .controller("SuccessfulLoanCtrl", SuccessfulLoanCtrl)
        .controller("BadLoanCtrl", BadLoanCtrl)
        .controller("UnmatchedLoanCtrl", UnmatchedLoanCtrl)
        .controller("RegisterconfirmCtrl", RegisterconfirmCtrl)
        .controller("LoanConfirmationCtrl", LoanConfirmationCtrl)
        .controller("LogoutCtrl", LogoutCtrl)
        .controller("RegistrationCtrl", RegistrationCtrl)
        .controller("ViewLoanCtrl", ViewLoanCtrl)
        .controller("ContractCtrl", ContractCtrl);

    function LandingCtrl($stateParams, $scope, $state) {
        var vm = this;
    }

    LandingCtrl.$inject = ["$stateParams", "$scope", "$state"];

    /*
    function ContractDetailsCtrl($stateParams, $state, $http) {
        var vm = this;
        vm.contract = {};
        vm.investors = {};
        vm.amount = 0;
        var loanId = $stateParams.loanId;

        $http.get("/api/loan/transactions/" + loanId)
            .then(function (result) {
                vm.totalinvestedTransactions = result.data;
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            });

        function getContractDetails(loanId) {
            $http.get("/api/contract/" + loanId)
                .then(function (result) {
                    vm.contract = result.data
                })
                .catch(function () {
                    alert("There was some problem fetching loan details");
                    $state.go("unmatchedloan")
                })
        }

        function getInvestorDetails(loanId) {
            $http.get("/api/investors/" + loanId)
                .then(function (result) {
                    vm.investors = result.data
                })
                .catch(function () {
                    alert("There was some problem fetching loan details");
                    $state.go("unmatchedloan")
                })
        }
    }

    ContractDetailsCtrl.$inject = ["$stateParams", "$state", "$http"];*/

    function LoginCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.login = function () {
            $http.post("/login", {
                email: vm.admin_users.email,
                password: vm.admin_users.password
            }).then(function (res) {
                $state.go("createloan")
            }).catch(function (err) {
                alert("Login was unsuccessful")
            })
        }
    }

    LoginCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];

    function LogoutCtrl($http, $q, $state) {
        var vm = this;

        $http.get("/logout", {}).then(function (res) {
            $state.go("landing")
        }).catch(function (err) {
            alert("Logout was unsuccessful");
        })
    }

    LogoutCtrl.$inject = ["$http", "$q", "$state"];


    function CreateLoanCtrl($stateParams, $scope, $state, loanService, $http) {
        var vm = this;
        vm.loan = {};

        $http.get("/api/buyers")
            .then(function (results) {
                console.info("----");
                vm.buyers = results.data;
                console.info("----" + vm.buyers);
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            });

        vm.createLoan = function () {
            console.info("loan info passed");
            loanService.createLoan({
                fv: vm.loan.fv,
                date_start: vm.loan.date_start,
                date_end: vm.loan.date_end,
                buyer_id: vm.loan.buyer_id,
                interest_rate: vm.loan.interest_rate,
                lc_no: vm.loan.lc_no,
                lc_bankname: vm.loan.lc_bankname,
                lc_bank_swiftcode: vm.loan.lc_bank_swiftcode,
                details: vm.loan.details,
                currency: vm.loan.currency
            })
                .then(function () {
                    vm.disabled = false;

                    // Flash.clear();
                    // Flash.create('success', "Successfully created loan request", 0, {
                    //     class: 'custom-class',
                    //     id: 'custom-id'
                    // }, true);
                    $state.go("landing");
                }).catch(function () {
                console.error("create loan has issue");
            });
        };
    }

    CreateLoanCtrl.$inject = ["$stateParams", "$scope", "$state", "loanService", "$http"];

    function LoanDetailsCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.loan = {};

        var loanId = $stateParams.loanId;

        $http.get("/api/loan/loandetails/" + loanId)
            .then(function (result) {
                vm.totalinvestedTransactions = result.data;
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            });

        function getLoanDetails(loanId) {
            $http.get("/api/loan/" + loanId)
                .then(function (result) {
                    vm.loan = result.data
                })
                .catch(function () {
                    alert("There was some problem fetching loan details");
                    $state.go("unmatchedloan")
                })
        }

        if (loanId) {
            getLoanDetails(loanId);
        } else {
            $state.go("loanconfirmation")
        }
    }

    LoanDetailsCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];

    function SuccessfulLoanCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.matchedLoans = [];

        vm.listLoan = function () {
            $http.get("/api/loan/successfulloan").then(function (result) {
                vm.matchedLoans = result.data;
            }).catch(function (err) {
                alert("No Loan Available")
            })
        };
        vm.listLoan();
    }

    SuccessfulLoanCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];


    function BadLoanCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.matchedLoans = [];

        vm.listLoan = function () {
            $http.get("/api/loan/badloan").then(function (result) {
                vm.matchedLoans = result.data;
            }).catch(function (err) {
                alert("No Loan Available")
            })
        };
        vm.listLoan();
    }

    BadLoanCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];

    function MatchedLoanCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.matchedLoans = [];

        vm.listLoan = function () {
            $http.get("/api/loan/listmatched").then(function (result) {
                vm.matchedLoans = result.data;
            }).catch(function (err) {
                alert("No Loan Available")
            })
        };
        vm.listLoan();
    }

    MatchedLoanCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];

    function UnmatchedLoanCtrl($stateParams, $scope, $state, $http) {
        var vm = this;
        vm.loans = [];

        vm.listLoan = function () {
            $http.get("/api/loan/list").then(function (result) {
                vm.loans = result.data;
            }).catch(function (err) {
                alert("No Loan Available")
            })
        };

        vm.listLoan();

        vm.searchLoan = function () {
            $http.post("/api/loan/search", {
                query: vm.query
            }).then(function (result) {
                vm.loans = result.data;
                console.info(vm.loans);
            }).catch(function (err) {
                alert("Can not find a loan")
            })
        }
    }

    UnmatchedLoanCtrl.$inject = ["$stateParams", "$scope", "$state", "$http"];

    function RegisterconfirmCtrl($stateParams, $scope, $state) {
        var vm = this;

        vm.register = function () {
            $state.go('regisconfirm', {});
        };
    }

    RegisterconfirmCtrl.$inject = ["$stateParams", "$scope", "$state"];

    function LoanConfirmationCtrl($stateParams, $scope, $state) {
        var vm = this;

        vm.register = function () {
            $state.go('regisconfirm', {});
        };
    }

    LoanConfirmationCtrl.$inject = ["$stateParams", "$scope", "$state"];

    function RegistrationCtrl($stateParams, $scope, $state, registerService) {
        var vm = this;
        vm.register = {};
        vm.register.companyType = "";
        vm.register.email = '';
        vm.register.password = "";
        vm.register.createdAt = "";
        vm.register.company = "";


        console.log("--- in RegistrationCtrl ---");
        vm.goRegister = function () {
            console.log("--- in vm.register.companyType ---" + vm.register.companyType);
            // send registration information to your DB service
            registerService.createUser(vm.register)
                .then(function (result) {
                        console.log("success! result: " + result);
                        $state.go('login');
                    }
                )
                .catch(
                    console.log("in catch")
                );
            console.log("email: " + vm.register.email);
        };
    }

    RegistrationCtrl.$inject = ["$stateParams", "$scope", "$state", "registerService"];

    function ViewLoanCtrl($stateParams, $state, $http) {
        var vm = this;
        vm.loan = {};

        vm.amount = 0;
        var loanId = $stateParams.loanId;

        $http.get("/api/loan/transactions/" + loanId)
            .then(function (result) {
                vm.totalinvestedTransactions = result.data;
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            });

        function getLoanDetails(loanId) {
            $http.get("/api/loan/" + loanId)
                .then(function (result) {
                    vm.loan = result.data
                })
                .catch(function () {
                    alert("There was some problem fetching loan details");
                    $state.go("unmatchedloan")
                })
        }

        if (loanId) {
            // get the loan details from backend
            getLoanDetails(loanId);
        } else {
            $state.go("loanconfirmation")
        }

        vm.investInLoan = function () {
            console.info(vm.loan);
            console.info(vm.loan.remain_amt);
            console.info(vm.amount);
            if (vm.amount < vm.loan.remain_amt && vm.amount > 0) {
                $http.post("/api/loan/" + loanId, {
                    reduced_amt: vm.amount
                }).then(function (result) {
                    getLoanDetails(loanId);
                    $http.get("/api/loan/transactions/" + loanId)
                        .then(function (result) {
                            vm.totalinvestedTransactions = 0;
                            console.info("vm.totalinvestedTransactions" + vm.totalinvestedTransactions);
                            vm.totalinvestedTransactions = result.data;
                            console.info("vm.totalinvestedTransactions" + vm.totalinvestedTransactions);

                        })
                        .catch(function () {
                            alert("There was some problem fetching loan details");
                            $state.go("unmatchedloan")
                        });

                }).catch(function (err) {
                    alert("Something went wrong while investing. Try again!");
                })
            } else {
                console.info("Can't invest a loan that is more than the remaining amount!");
                alert("Can't invest a loan that is more than the remaining amount!");
            }
        }
    }

    ViewLoanCtrl.$inject = ["$stateParams", "$state", "$http"];


    ContractCtrl.$inject = ["$stateParams", "$state", "$http"];
    function ContractCtrl($stateParams, $state, $http) {
        var vm = this;
        var loanId = $stateParams.loanId;
        console.info(loanId);
        console.info("ContractCtrl -----");

        $http.get("/api/contract/" + loanId)
            .then(function (result) {
                vm.contract = result.data
                console.info(vm.contract);
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            })

        $http.get("/api/investors/" + loanId)
            .then(function (result) {
                vm.investedAmts = result.data
                console.info(vm.contract);
            })
            .catch(function () {
                alert("There was some problem fetching loan details");
                $state.go("unmatchedloan")
            })
    }


})();
